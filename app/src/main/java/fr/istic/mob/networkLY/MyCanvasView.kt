package fr.istic.mob.networkLY

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.graphics.*
import android.text.InputType
import android.util.AttributeSet
import android.view.GestureDetector
import android.view.MotionEvent
import android.view.View
import android.widget.EditText
import android.widget.NumberPicker
import com.github.dhaval2404.colorpicker.MaterialColorPickerDialog
import fr.istic.mob.networkLY.Figure.Type


class MyCanvasView : View {

    private var figures = mutableListOf<Figure?>()
    private var typefigure = Type.NULL

    var menu = false;
    var move = false;

    //val paint = Paint()

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        var app: Graph = context.applicationContext as Graph
        figures = app.getFigures()

    }

    // on dessine d'abord les connexion puis les object
    override fun onDraw(canvas: Canvas) {
        for (figure in figures) {
            if (figure != null) {
                if(figure.con) {
                    figure.draw(canvas)
                }
            }
        }
        for (figure in figures) {
            if (figure != null) {
                if(figure.obj) {
                    figure.draw(canvas)
                }
            }
        }
    }

    /**
     *  menu OBJECT/CONNECTION
     */

    private fun menuObject(figure: Figure) {
        val builder = AlertDialog.Builder(getContext())
        builder.setNegativeButton(R.string.cancel, object : DialogInterface.OnClickListener {
            override fun onClick(dialog: DialogInterface, which: Int) {
                dialog.cancel()
            }
        })
        builder.setTitle(R.string.menu_object)
            .setItems(R.array.menu_object, DialogInterface.OnClickListener { dialog, which ->
                if (which == 0) {
                    removeObject(figure)
                } else if (which == 1) {
                    changeName(figure)
                } else if (which == 2) {
                    colorPicker(figure)
                } else if (which == 3) {
                    changeImage(figure)

                }
            })
        builder.create()
        builder.show()
        }

    private fun menuConnexion(figure: Figure) {
        val builder = AlertDialog.Builder(getContext())
        builder.setNegativeButton(R.string.cancel, object : DialogInterface.OnClickListener {
            override fun onClick(dialog: DialogInterface, which: Int) {
                dialog.cancel()
            }
        })
        builder.setTitle(R.string.menu_connexion)
            .setItems(R.array.menu_connexion, DialogInterface.OnClickListener { dialog, which ->
                if (which == 0) {
                    figures.remove(figure)
                    invalidate()
                } else if (which == 1) {
                    changeName(figure)
                } else if (which == 2) {
                    colorPicker(figure)
                } else if (which == 3) {
                    changeThickness(figure)
                }
            })
        builder.create()
        builder.show()
    }

    private fun changeName(figure: Figure)
    {
        val builder = AlertDialog.Builder(getContext())
        builder.setTitle(R.string.change_name)
        val input = EditText(getContext())
        input.setInputType(InputType.TYPE_CLASS_TEXT)
        builder.setView(input)
        builder.setPositiveButton("OK", object : DialogInterface.OnClickListener {
            override fun onClick(dialog: DialogInterface, which: Int) {
                val newName = input.getText().toString()
                figure.set_Name(newName)
            }
        })
        builder.setNegativeButton(R.string.cancel, object : DialogInterface.OnClickListener {
            override fun onClick(dialog: DialogInterface, which: Int) {
                dialog.cancel()
            }
        })
        builder.show()
    }

    private fun colorPicker(figure: Figure){
        MaterialColorPickerDialog
            .Builder(getContext()) // Pass Activity Instance
            .setTitle(R.string.pick_a_color)
            .setColorRes(resources.getIntArray(R.array.colors).toList())   	// Default ColorShape.CIRCLE
            .setColorListener { color, colorHex ->
                // Handle Color Selection
                if (colorHex.equals("#ff0000")){
                    figure.set_color(-0x10000)
                    invalidate()
                }
                if (colorHex.equals("#008000")){
                    figure.set_color(-0xff0100)
                    invalidate()
                }
                if (colorHex.equals("#0000ff")){
                    figure.set_color(-0xffff01)
                    invalidate()
                }
                if (colorHex.equals("#ffff00")){
                    figure.set_color(-0x100)
                    invalidate()
                }
                if (colorHex.equals("#2bfafa")){
                    figure.set_color(-0xff0001)
                    invalidate()
                }
                if (colorHex.equals("#ff00ff")){
                    figure.set_color(-0xff01)
                    invalidate()
                }
                if (colorHex.equals("#000000")){
                    figure.set_color(-0x1000000)
                    invalidate()
                }
            }
            .show()
    }

    private fun changeThickness(figure: Figure){
        val builder = AlertDialog.Builder(getContext())
        builder.setTitle(R.string.change_name)
        val numberPicker = NumberPicker(getContext());
        numberPicker.setMaxValue(20);
        numberPicker.setMinValue(1);
        builder.setView(numberPicker);
        builder.setTitle(R.string.change_thickness);
        builder.setPositiveButton("OK", object : DialogInterface.OnClickListener {
            override fun onClick(dialog: DialogInterface, which: Int) {
                println(numberPicker.getValue())
                figure.set_thickness(numberPicker.getValue())
                invalidate()
            }
        })
        builder.setNegativeButton(R.string.cancel, object : DialogInterface.OnClickListener {
            override fun onClick(dialog: DialogInterface, which: Int) {
                dialog.cancel()
            }
        })
        builder.create();
        builder.show();
    }

    private fun changeImage (figure: Figure){
        val builder = AlertDialog.Builder(getContext())
        builder.setNegativeButton(R.string.cancel, object : DialogInterface.OnClickListener {
            override fun onClick(dialog: DialogInterface, which: Int) {
                dialog.cancel()
            }
        })
        builder.setTitle(R.string.change_image)
            .setItems(R.array.change_image_menu, DialogInterface.OnClickListener { dialog, which ->
                if (which == 0) {
                    val bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.printer)
                    figure.set_bitmap(bitmap)
                    invalidate()
                } else if (which == 1) {
                    val bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.phone)
                    figure.set_bitmap(bitmap)
                    invalidate()
                } else if (which == 2) {
                    val bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.tv)
                    figure.set_bitmap(bitmap)
                    invalidate()
                }
            })
        builder.create()
        builder.show()

    }

    /**
     * Clique de l'écran
     */
    override fun onTouchEvent(event: MotionEvent): Boolean {
        val x = event.x
        val y = event.y

        when (event.action) {
            MotionEvent.ACTION_DOWN -> {
                if (typefigure == Type.CONNECTION) {
                    actionDownConnection(x, y)
                } else if (typefigure == Type.OBJECT) {
                    gestureDetectorObject.onTouchEvent(event)
                } else if (move) {
                    actionDownMove(x, y)
                } else if (menu) {
                    actionDownMenu(x, y)
                }
            }
            MotionEvent.ACTION_MOVE -> {
                if (typefigure == Type.CONNECTION) {
                    actionMoveConnection(x, y)
                } else if (move) {
                    actionMove(x, y)
                }
            }
            MotionEvent.ACTION_UP -> {
                if (typefigure == Type.CONNECTION) {
                    actionUpConnection(x, y)
                } else if (move) {
                    actionUp()
                }
            }
        }
        invalidate()
        return true
    }

    /**
     * Clique de l'écran pour lancer les menus
     */

    private fun actionDownMenu(x: Float, y: Float) {
        for (i in figures) {
            if (i != null) {
                if( i.rect.contains(x, y)){
                    menuObject(i)
                    return
                }
                //Si on clique dans une zone autour du millieu de la connexion
                else if ((i.xmillieu > 0 && i.ymillieu > 0) && (x >= i.xmillieu-50 && x <= i.xmillieu+50) && (y >= i.ymillieu-50 && y <= i.ymillieu+50)){
                    menuConnexion(i)
                    return
                }
            }
        }
    }

    /**
      * Event modifer un OBJECT/CONNECTION
      */
    private fun actionDownMove(x: Float, y: Float) {
        for (i in figures) {
            if (i != null) {
                if( i.rect.contains(x, y)){
                    i.verrouillage(true)
                    return
                }
                //Si on clique dans une zone autour du millieu de la connexion
                else if ((i.xmillieu > 0 && i.ymillieu > 0) && (x >= i.xmillieu-50 && x <= i.xmillieu+50) && (y >= i.ymillieu-50 && y <= i.ymillieu+50)){
                    i.verrouillage(true)
                    return
                }
            }
        }
    }

    private fun actionMove(x: Float, y: Float) {
        for (i in figures) {
            if (i != null) {
                if(i.verrou && i.obj){
                    val xCenter = i.rect.centerX()
                    val yCenter = i.rect.centerY()
                    i.setReference(x, y)
                    for (j in figures){
                        if (j != null && j.con) {
                            if (j.x1 == xCenter && j.y1 == yCenter) {
                                j.setReference(i.rect.centerX(), i.rect.centerY())
                            }else if(j.x2 == xCenter && j.y2 == yCenter){
                                j.setCoin(i.rect.centerX(), i.rect.centerY())
                            }
                        }
                    }
                    return
                }else if ( i.verrou && i.con){
                        i.setAngle(x, y)
                        return
                }
            }
        }
    }

    private fun actionUp() {
        for (i in figures) {
            if (i != null) {
                if(i.verrou){
                    i.verrouillage(false)
                    return
                }
            }
        }
    }

    /**
     * Event CONNECTION
     */
    private fun actionDownConnection(x: Float, y: Float){
        if(figures.isEmpty()){
            return
        }else {
            for (i in figures) {
                if (i != null) {
                    if (i.rect.contains(x, y)) {
                        val figure = Figure.creer(typefigure)
                        if (figure != null) {
                            figure.setReference(i.rect.centerX(), i.rect.centerY())
                            figure.verrouillage(true)
                            figures.add(figure)
                            return
                        }
                    }
                }
            }
            return
        }
    }

    private fun actionMoveConnection(x: Float, y: Float) {
        for (i in figures) {
            if (i != null) {
                if (i.verrou) {
                    i.setCoin(x, y)
                    return
                }
            }
        }
    }

    private fun actionUpConnection(x: Float, y: Float) {
        if(figures.isEmpty()){
            return
        }else {
            for (i in figures) {
                if (i != null) {
                    if (i.rect.contains(x, y)) {
                        figures.last()?.setCoin(i.rect.centerX(), i.rect.centerY())
                        figures.last()?.verrouillage(false)
                        addNameConnexion()
                        return
                    }
                }
            }
            if (figures.last()?.verrou!! == true) {
                figures.removeLast()
                return
            }
        }
    }

    private fun addNameConnexion()
    {
        val builder = AlertDialog.Builder(getContext())
        builder.setTitle(R.string.connexion_name)
        val input = EditText(getContext())
        input.setInputType(InputType.TYPE_CLASS_TEXT)
        builder.setView(input)
        builder.setPositiveButton("OK", object : DialogInterface.OnClickListener {
            override fun onClick(dialog: DialogInterface, which: Int) {
                val connexionName = input.getText().toString()
                println(connexionName)
                figures.last()?.set_Name(connexionName)
            }
        })
        builder.setNegativeButton(R.string.cancel, object : DialogInterface.OnClickListener {
            override fun onClick(dialog: DialogInterface, which: Int) {
                dialog.cancel()
                typefigure = Type.CONNECTION
                figures.removeLast()
            }
        })
        builder.show()
    }

    /**
     *Long press pour créer un OBJECT
     */
    val gestureDetectorObject = GestureDetector(object : GestureDetector.SimpleOnGestureListener() {
        override fun onLongPress(event: MotionEvent) {
            var xCoor = event.x
            var yCoor = event.y
            when (event.getAction()) {
                MotionEvent.ACTION_DOWN -> {
                    val builder = AlertDialog.Builder(getContext())
                    builder.setTitle(R.string.object_name)
                    val input = EditText(getContext())
                    input.setInputType(InputType.TYPE_CLASS_TEXT)
                    builder.setView(input)
                    builder.setPositiveButton("OK", object : DialogInterface.OnClickListener {
                        override fun onClick(dialog: DialogInterface, which: Int) {
                            val rectangleName = input.getText().toString()
                            typefigure = Type.OBJECT
                            val figure = Figure.creer(typefigure)
                            if (figure != null) {
                                figure.setReference(xCoor, yCoor)
                                figure.set_Name(rectangleName)
                                figures.add(figure)
                            }
                        }
                    })
                    builder.setNegativeButton(
                        R.string.cancel,
                        object : DialogInterface.OnClickListener {
                            override fun onClick(dialog: DialogInterface, which: Int) {
                                dialog.cancel()
                                typefigure = Type.OBJECT
                            }
                        })
                    builder.show()
                    invalidate()
                }
            }
        }
    })

    /**
     * définit le type de la prochaine figure à dessiner
     * @param type
     */
    fun setTypeFigure(type: Type) {
        typefigure = type
    }

    /**
     * supprime tout les figure et rénitialise le Canvas
     * @param type
     */
    fun clear() {
        figures.clear()
        invalidate()
    }

    fun removeObject(figure: Figure){
        for (i in figures) {
            if (i != null) {
                if (i.con){
                    if((i.x1 == figure.rect.centerX() && i.y1 == figure.rect.centerY()) ||(i.x2 == figure.rect.centerX() && i.y2 == figure.rect.centerY()))
                        figures.remove(i)
                }
            }
        }
        figures.remove(figure)
        invalidate()
    }

/*
    fun getFigure(): MutableList<Figure?> {
        return figures
    }

    fun save() {
    //val text: String = mEditText.getText().toString()
    var fos: FileOutputStream? = null
    var oos: ObjectOutputStream? = null
    try {
        fos = context.openFileOutput(FILE_NAME, MODE_PRIVATE)
        oos = ObjectOutputStream(fos)
       /* for (i in figures)
        {
            oos?.writeObject(i)
        }*/
        oos?.writeObject(graph?.getFigures())

        fos.close()
        oos.close()

        Toast.makeText(context,"file saved", LENGTH_LONG).show()

    } catch (e: FileNotFoundException) {
        e.printStackTrace()
    } catch (e: IOException) {
        e.printStackTrace()
    }
}

fun load() {

    val fis = context.openFileInput(FILE_NAME)
    val `is` = ObjectInputStream(fis)
    val simpleClass: MyCanvasView = `is`.readObject() as MyCanvasView
    `is`.close()
    fis.close()
}

*/

}
