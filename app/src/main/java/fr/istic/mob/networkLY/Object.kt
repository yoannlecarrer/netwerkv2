package fr.istic.mob.networkLY

import android.content.res.Resources
import android.graphics.*


class Object : Figure{

    override var paint = Paint()
    val paintName = Paint()
    val paintBitmap = Paint()

    constructor() : super(){
        init()
    }

    fun init() {
        paint.style = Paint.Style.FILL_AND_STROKE
        paintName.setColor(Color.BLUE)
        paint.color = color
        paintName.setTextAlign(Paint.Align.RIGHT)
        paintName.setTextSize(50F)
        paintName.setTypeface(Typeface.create("Arial", Typeface.BOLD))
        obj = true
    }

    override fun draw(canvas: Canvas) {
        paint.color = color
        rect = RectF(x1, y1, x1 + 110f, y1 + 110F)
        if(x2!=null) {
            rect = RectF(x2!!, y2, x2!! + 110f, y2 + 110F)
        }
        canvas.drawText(name, x1, y1 + 55, paintName)
        canvas.drawRect(rect, paint)
        if(bitmapBool){
            canvas.drawBitmap(bitmap, x1, y1, paintBitmap);
        }

    }
}