package fr.istic.mob.networkLY

//import dev.sasikanth.colorsheet.ColorSheet
import android.Manifest
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.gson.Gson
import fr.istic.mob.networkLY.Figure.Type
import java.io.*


class MainActivity : AppCompatActivity() {

    lateinit var view:MyCanvasView
    private val FILE_NAME = "screenshot.png"
    lateinit var myBitmap: Bitmap

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)

        view = MyCanvasView(this, null)
        setContentView(R.layout.activity_main)
        val linearLayout = findViewById<LinearLayout>(R.id.layout)
        linearLayout.addView(view)
        //setSupportActionBar(findViewById(R.id.toolbar))

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        return when (item.itemId) {
            R.id.action_addObjects -> {
                view.setTypeFigure(Type.OBJECT)
                view.menu = false
                view.move = false
                println("test")
                Toast.makeText(applicationContext, R.string.text_add_Objects, Toast.LENGTH_LONG)
                        .show()
                return true
            }
            R.id.action_addConnections -> {
                view.setTypeFigure(Type.CONNECTION)
                view.menu = false
                view.move = false
                Toast.makeText(applicationContext, R.string.text_add_Connection, Toast.LENGTH_LONG)
                        .show()
                return true
            }
            R.id.action_editConnectionsObjects -> {
                view.setTypeFigure((Type.NULL))
                view.menu = true
                view.move = false
                Toast.makeText(applicationContext, R.string.text_edit_Connection, Toast.LENGTH_LONG)
                        .show()
                return true
            }
            R.id.action_MoveConnectionsObjects -> {
                view.setTypeFigure((Type.NULL))
                view.menu = false
                view.move = true
                Toast.makeText(applicationContext, R.string.text_edit_Connection, Toast.LENGTH_LONG)
                        .show()
                return true
            }
            R.id.action_send_mail -> {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (ContextCompat.checkSelfPermission(
                                    this,
                                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                            ) != PackageManager.PERMISSION_GRANTED
                    ) {
                        ActivityCompat.requestPermissions(
                                this,
                                arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                                100
                        )
                    } else {
                        myBitmap = takeScreenshot(view)
                        saveBitmap(myBitmap)
                    }
                } else {
                    myBitmap = takeScreenshot(view)
                    saveBitmap(myBitmap)
                }
                return true
            }
            R.id.action_display_interne -> {
                Toast.makeText(applicationContext, R.string.action_display_interne, Toast.LENGTH_LONG).show()
                return true
            }
            R.id.action_save_interne -> {
                Toast.makeText(applicationContext, R.string.action_save_interne, Toast.LENGTH_LONG)
                        .show()
                return true
            }
            R.id.action_import_plan -> {
                selectOtherPlan(this)
                Toast.makeText(applicationContext, R.string.action_import_plan, Toast.LENGTH_LONG)
                        .show()
                return true
            }
            R.id.action_reset -> {
                view.clear()
                view.setTypeFigure((Type.NULL))
                view.menu = false
                view.move = false
                Toast.makeText(applicationContext, R.string.text_reset, Toast.LENGTH_LONG).show()
                return true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onRequestPermissionsResult(
            requestCode: Int,
            permissions: Array<out String>,
            grantResults: IntArray
    ) {
        if (requestCode==100){
            if (grantResults.size>0&&grantResults[0]== PackageManager.PERMISSION_GRANTED){
                myBitmap=takeScreenshot(view)
                saveBitmap(myBitmap)
            }else{
                Toast.makeText(this, R.string.permission_not_granted, Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun saveBitmap(bitmap: Bitmap) {

        var filePath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        val imagePath = File(filePath, FILE_NAME)
        var fos: FileOutputStream
        try {
            fos = FileOutputStream(imagePath)
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos)
            fos.flush()
            fos.close()
            Toast.makeText(
                    this,
                    "La capture est enregistrée dans:" + System.getProperty("line.separator") + imagePath.toString(),
                    Toast.LENGTH_LONG
            ).show()
            sendMail(imagePath)
        } catch (e: FileNotFoundException) {
            Log.e("GREC", e.message, e)
        } catch (e: IOException) {
            Log.e("GREC", e.message, e)
        }
    }

    fun sendMail(filePath: File) {
        val emailIntent = Intent(Intent.ACTION_SEND)
        emailIntent.putExtra(Intent.EXTRA_EMAIL, arrayOf("mikhail.foursov@irisa.fr"))
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Envoie capture")
        emailIntent.putExtra(Intent.EXTRA_TEXT, "Ceci est un envoie de capture d'écran de NetwerkLY")
        emailIntent.type = "image/png"
        val myUri: Uri = Uri.parse(filePath.toString())
        if (myUri!=null)
        {
            emailIntent.putExtra(Intent.EXTRA_STREAM, myUri)
        }else{
            Toast.makeText(this, R.string.uri_null, Toast.LENGTH_LONG).show()
        }
        startActivity(Intent.createChooser(emailIntent, R.string.send_mail.toString()))
    }


    fun takeScreenshot(v: MyCanvasView): Bitmap {
        // create bitmap screen capture
        val v1 = window.decorView.rootView
        v1.isDrawingCacheEnabled = true
        val bitmap = Bitmap.createBitmap(v1.drawingCache)
        v1.isDrawingCacheEnabled = false
        return bitmap
    }


    open fun selectOtherPlan(context: Context) {
        val options = arrayOf<CharSequence>("Prendre une photo", "Choisir depuis la Gallery", "Annuler")
        val builder = AlertDialog.Builder(context)
        builder.setTitle(R.string.choose_other_plan)
        builder.setItems(options) { dialog, item ->
            if (options[item]=="Prendre une photo") {
                val takePicture = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                startActivityForResult(takePicture, 0)
            } else if (options[item]== "Choisir depuis la Gallery") {
                val pickPhoto = Intent(
                        Intent.ACTION_PICK,
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                )
                startActivityForResult(pickPhoto, 1)
            } else if (options[item]=="Annuler") {
                dialog.dismiss()
            }
        }
        builder.show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode != RESULT_CANCELED) {
            when (requestCode) {
                0 -> if (resultCode == RESULT_OK && data != null) {
                    val selectedImage = data.extras!!["data"] as Bitmap?
                    val d: Drawable = BitmapDrawable(resources, selectedImage)
                    view.background = d
                }
                1 -> if (resultCode == RESULT_OK && data != null) {
                    val selectedImage = data.data
                    val filePathColumn = arrayOf(MediaStore.Images.Media.DATA)
                    if (selectedImage != null) {
                        val cursor: Cursor? = contentResolver.query(
                                selectedImage,
                                filePathColumn, null, null, null
                        )
                        if (cursor != null) {
                            cursor.moveToFirst()
                            //val columnIndex: Int = cursor.getColumnIndex(filePathColumn[0])
                            // val picturePath: String = cursor.getString(columnIndex)
                            val bg: Drawable?
                            bg = try {
                                val inputStream: InputStream? = contentResolver.openInputStream(
                                        selectedImage
                                )
                                Drawable.createFromStream(inputStream, selectedImage.toString())
                            } catch (e: FileNotFoundException) {
                                ContextCompat.getDrawable(this, R.drawable.ic_launcher_background)
                            }
                            view.background = bg
                            cursor.close()
                        }
                    }
                }
            }
        }
    }

}