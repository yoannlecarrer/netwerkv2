package fr.istic.mob.networkLY

import android.graphics.*


abstract class Figure {

    var x1: Float = 0f
    var x2: Float? = null
    var y1: Float = 0f
    var y2: Float = 0f

    var xmillieu=0F
    var ymillieu=0F

    var xangle=0F
    var yangle=0F

    var rect = RectF()
    val path = Path()

    var thickness:Int = 10
    var color:Int = -0xff01
    var name:String=""
    var bitmapBool:Boolean = false
    lateinit var bitmap:Bitmap

    var obj = false
    var con = false

    var verrou = false

    // couleur tirée aléatoirement lors de sa création
    protected open var paint = Paint()

    enum class Type {
        // TODO ajouter les symboles LIGNE et ELLIPSE
        OBJECT, CONNECTION,NULL
    }

    companion object {
        fun creer(type: Type): Figure? {
            when (type) {
                Type.OBJECT -> return Object()
                Type.CONNECTION -> return Connection()
                Type.NULL -> return null
            }
            return null
        }
    }

    /**
     * retourne la couleur de dessin de cette figure
     * @return Paint de la figure
     */


    // point de départ
    open fun setReference(x: Float, y: Float) {
        x1 = x
        y1 = y
        rect = RectF(x1, y1, x1 + 110, y1 + 110)
    }

    open fun set_Name(new_name: String) {
        name=new_name
    }

    open fun set_color(new_color: Int) {
        color=new_color
    }
    
    open fun set_thickness(new_thickness: Int){
        thickness = new_thickness
    }

    open fun set_bitmap(new_bitmap: Bitmap){
        bitmapBool=true
        bitmap = Bitmap.createScaledBitmap(new_bitmap,110,110,true)
    }

    //verouille l'object/connection
    open fun verrouillage(b: Boolean){
        verrou = b
    }

    //point d'arriver
    open fun setCoin(x: Float, y: Float) {
        x2 = x
        y2 = y
    }

    open fun setAngle(x: Float, y: Float) {
        xangle=x
        yangle=y
    }

    /**
     * Cette méthode dessine la figure sur le canvas
     * Cette méthode est spécifique du type exact de la figure
     * @param canvas
     */
    abstract fun draw(canvas: Canvas)

}