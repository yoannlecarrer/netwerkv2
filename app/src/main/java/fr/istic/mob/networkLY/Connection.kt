package fr.istic.mob.networkLY

import android.graphics.*

class Connection : Figure{

    override var paint = Paint()
    val paintName = Paint()

    constructor(): super(){
        init()
    }

    fun init() {
        color = -0xff0001
        paint.style = Paint.Style.FILL
        paint.color = color
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(thickness.toFloat());
        paintName.setColor(Color.BLUE)
        paintName.setTextAlign(Paint.Align.RIGHT)
        paintName.setTextSize(50F)
        paintName.setTypeface(Typeface.create("Arial", Typeface.BOLD))
        con = true
    }

    override fun draw(canvas: Canvas) {
        paint.setStrokeWidth(thickness.toFloat());
        paint.color = color
        if(x2!=null)
        {
            path.reset()

            path.moveTo(x1,y1)

            xmillieu = (x1 + x2!!)/2
            ymillieu = (y1+y2)/2

            // on initialise au début pour faire une ligne
            if(xangle==0F && yangle ==0F){
                xangle=x1
                yangle=y1
                path.quadTo(xangle, yangle, x2!!, y2)
            }

            // ensuite on peut courber
            else{
                xmillieu = (xangle + x2!!)/2
                ymillieu = (yangle+y2)/2
                path.quadTo(xangle, yangle, x2!!, y2)
            }
        }
        canvas.drawPath(path,paint)
        canvas.drawText(name,xmillieu,ymillieu,paintName)

    }

}